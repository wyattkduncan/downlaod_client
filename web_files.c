#include <stdio.h>
#include <readline/history.h>
#include <readline/readline.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>

void menu();
char get_choice();
void download_file(int s);
void list_files(int s);
void close_connection(int s);
int open_connection();

int main()
{
    int sockfd = open_connection();
    while(1)
    {
        menu();
        char c = get_choice();
        switch (c)
        {
            case 'l':
            case 'L':
                list_files(sockfd);
                break;
            case 'd':
            case 'D':
                download_file(sockfd);
                break;
            case 'q':
            case 'Q':
                close_connection(sockfd);
                exit(0);
            default:
                printf("Please enter d, l, or q\n");
                break;
        }
    }
}

void menu()
{
    printf("L)ist files\n");
    printf("D)ownload a file\n");
    printf("Q)uit\n");
    printf("\n");
}

char get_choice()
{
    char *line = readline("Choice: ");
    char first = line[0];
    free(line);
    return first;
}

int open_connection()
{
    // gethostbyname
    struct sockaddr_in sa;
    int sockfd;
    
    printf("Looking up IP address...  ");
    struct hostent *he = gethostbyname("runwire.com");
    struct in_addr *ip = (struct in_addr*)he->h_addr_list[0];
    printf("IP address is %s\n", inet_ntoa(*ip));
    
    // Fill in socket struct
    printf("Creating Structure\n");
    sa.sin_family =AF_INET;
    sa.sin_port = htons(1234);
    sa.sin_addr = *((struct in_addr *)ip);
    
    // Create socket
    printf("creating socket\n");
    sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(sockfd==-1){
        fprintf(stderr, "Cant Create socket\n ");
        exit(3);
    }
    
    // Connect
    printf("Connecting\n");
    int res = connect(sockfd, (struct sockaddr *) &sa, sizeof(sa));
    if(res == -1){
        fprintf(stderr, "Cant connect\n");
        exit(2);
    }
    
    char buf[10];
    int rsize = recv(sockfd, buf, 100, 0);
    
    // return socket number (file descriptor)
    return sockfd;
}

void list_files(int s)
{
    // Send LIST command
    //printf("LIST\n");
    char buf[1000];
    char result[10];
    size_t rsize;
    printf("Listing...\n");
    
    sprintf(buf, "LIST\n");
    send(s, buf, strlen(buf),0);
    
    // Receive listing
    recv(s, buf, 1000, 0);
    
    
    // Display to user
    printf("%s", buf);
}

/*void download_file(int s)
{
    // Ask user which file
    char *line = readline("Which File? ");
    char buf[1000];
    
    // Find out how big the file is
    size_t size =0;
    int count =0;
    //size_t size;
    
    sprintf(buf, "SIZE %s\n", line);
    send(s, buf, strlen(buf),0);
    recv(s, buf, 1000, 0);
    printf("%s\n", buf);
    
    int success = sscanf(buf, "+OK %zu", &size);
    if (success == 1){
        printf("size is: %zu\n", size);    
        
        // Send GET command
        printf("getting\n");
        sprintf(buf, "GET %s", line);
        send(s, buf, strlen(buf),0);
        printf("got\n");
        
        // Receive data
        while((int)size >= 0)
        {
        
        fwrite(buf, size, 1, stdout);
        recv(s, buf, 1000, 0);
        size =1000;
        }
        
    }
    else {
        printf("error\n");
    }
    // Display to user
    free(line);
    free(buf);
}*/




void download_file(int s)
{
    
    char buf[1000];
    int result;
    size_t rsize = 1;
    
    // Ask user which file
    char *line = readline("Choose a file: ");
    // Find out how big the file is
    sprintf(buf, "SIZE %s\n", line);
    
    
    
    
    send(s, buf, strlen(buf), 0);
    printf(" SENDING: %s", buf);
    recv(s, buf, 1000, 0);
    printf(" RECEIVING: %s", buf);
 
    
    sscanf(buf, "+OK %d", &result);
    printf(" RESULT: %d\n", result);
   
    sprintf(buf, "GET %s\n", line);
    
    
    send(s, buf, strlen(buf), 0);
    printf(" SENDING: %s", buf);
    // Receive data
    for (int i = 0; i < 80; i++)
    {
        buf[i]='\0';
    }
    
    recv(s, buf, 4, 0);
    
    printf(" RECEIVING: %s", buf);
    
    if (strncmp(buf, "+", 1) == 0) { 
        FILE *z;
            char filename[50];
            sprintf(filename, "%s", line);
            z = fopen(filename, "a");
        
        do  {
            rsize = recv(s, buf, 1000, 0);   
            fwrite(buf , rsize , 1 , z );
            result -= rsize;
            printf("result = %d\n", result);
            if (rsize == 0) {
                break;
            }
        } while (result > 1000);
        fclose(z);
        
    }
    else {
        printf(" RECEIVING: %s", buf);
    }
    
    free(line);
}



void close_connection(int s)
{
    // send QUIT
    char Q[100];
    printf("Quitting...\n");
    sprintf(Q, "QUIT\n");
    send(s, Q, strlen(Q),0);
    // Close socket
    close(s);
}